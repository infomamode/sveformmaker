# SVEFormMaker #

## **1 - Description :**
- - -

SVEFormMaker est une application PHP qui permet de générer des formulaires. Il peut être intégré de manière autonome dans un flux de numérisation de processus qui s’appuie sur un logiciel de GED (principalement ELISE).

La description des formulaires ce fait via un fichier JSON. Vous pouvez spécifier un certain nombre de types de champ et définir l'action du formulaire.

### 1.1 - Structure des dossiers de l’application
- - -

L’application est structurée de la façon suivante :

Le dossier ***« sve »*** est le dossier racine d’installation

*   **« app »** est le dossier racine du module
*   **« app/config »**
    contient le fichier dotEnv (.env) de configuration du module
    
*   **« app/forms »**
    Contient les fichiers JSON qui définissent les formulaires à appeler. L’appel d’un formulaire spécifique est liée à son nom (slug)

*   **« app/public »**
    Contient les ressources accessibles au public

*   **« app/storage/data »**
    Répertoire de stockage de fichiers liées au formulaire. Ex : les descriptifs des exigences de poste selon le numéro d’offre

*   **« app/tmp »**
    Contient le PDF généré durant l’envoi du formulaire, ce dossier doit être accessible en écriture.

*   **« app/vendor »**
    Répertoire des librairies installé via Composer

## **2 - Installation**
- - -

### 2.1 - Pré-requis :

*   Langage de programmation

    *   PHP 5.6 +

*   Système

    *   Debian 7 + ou RHEL 7+
    *   Curl
    *   Ghoscript
    *   Apache 2.4
    *   Module php-json

### 2.2 - Configuration système
- - -

> Les commandes ont pour référence l'environnement Debian 8

####  a) - Cloner le repository Git


```
cd /var/www/html
git clone https://mamode-djae@bitbucket.org/infomamode/sveformmaker.git
```

####  b) - Sécuriser l’accès à la gestion et fichiers temporaires
Pour un niveau de sécurite raisonnable penser à vérifier les points suivants :
Configuer votre serveur web pour n'accéder via url public au dossier "app/public" Ex: sve.domaine.fr.

Les autres dossiers de l'application devrait être stocker en dehors du répertoire web par défaut (/var/www).
Mettre un lien symbolique dans le repertoire web pour le dossier "public" et "manage"
Mettre un fichier *.htaccess* dans le dossier **« app/manage »** pour protéger l’accès.
Configurer une url interne pour l'accès à l'interface de gestion Ex : extranet.domaine.fr/sve.
Mettre en place les régles firewall afin d'en limiter l'accès aux IP autorisées.

L'accès à l'interface de gestion n'est disponible que 
si un fichier UNLOCK est présent dans le dossier **« app/manage »**. le fichier peut être vide.
L'appel de la page doit provenir d'une plage d'IP autorisé ( voir le fichier de configuration pour préciser l'adresse IP) sinon l'affichage est bloqué par le code.

####  c) - Ajuster les droits et les propriétaires


Pour  les dossiers « app/ », « app/forms » et « app/storage »

```
sudo mkdir -p sveformmaker/app/tmp 
sudo chown -R www-data:www-data sveformmaker/  
sudo chmod -R 755 sveformmaker/app/tmp
sudo chmod -R 755 sveformmaker/app/forms
sudo chmod -R 755 sveformmaker/app/storage
```

####  d) - Si vous n'avez pas déjà une installation de composer

Vous pouvez installer Composer provisoirement en local, exécutez le programme d'installation dans le répertoire de l’application « app/ ». 

```
cd /var/www/html/svefrommaker/app
curl -sS https://getcomposer.org/installer | php
php composer.phar install
```

### 2.3 - Configuration de l’application
---------------------------------------------------------

### Fichier .env

**Le fichier .env** contient tout les valeurs de configuration de l’application et ces données sont accessibles via **$\_ENV\[‘nom\_de\_valeur’\]**
Dupliquer le ficher **app/config/.env-dist** et renommer le fichier **.env** pour configurer votre installation.

```

 TITLE_VIEW_FORM="Formulaire de saisine"
 TITLE_PDF="Formulaire de saisine"
 DEFAULT_FORM="default"
 APP_PATH="/var/www/dev/sve/app/"
 WEB_PATH="../dev/sve-beta/"
 SUPPORT_MAIL="webmaster@ac-reunion.fr"
 ID_ACADEMIE="AC-REUNION"
 URL_LOGO_ACADEMIE='<https://www.ac-reunion.fr/fileadmin/ANNEXES-ACADEMIQUES/CHARTE-GRAPHIQUE/logos-academie/logo-reunion-vert-horizontal-marianne.jpg>'
 SECUREIP=192.168.1.0
 ## By default send AR for user*

 SEND_AR="true"
 ## APP_MODE possible value 'dev', 'prod'
 APP_MODE="dev"
 TIMEZONE="Indian/Reunion"
 NBR_ATTACHMENTS=5
 MAX_SIZE_FILES=2097152

### Base de données de production
-----------------------------------------------------

 P_DB_HOST="localhost"
 P_DB_USER="johndoe"
 P_DB_PASSWORD="aChangerDeSuite"
 P_DB_NAME="sveacad"

### Base de developpement
-----------------------------------------------------

 D_DB_HOST="localhost"
 D_DB_USER="janedoe"
 D_DB_PASSWORD="aChangerDeSuite"
 D_DB_NAME="sveacad-dev"

### Server Mail
-----------------------------------------------------

 MAIL_SMTP_AUTH="true"
 MAIL_SMTP_HOST="smtps.ac-reunion.fr"
 MAIL_SMTP_PORT="465"
 GEC_MAIL_FROM="gec@ac-reunion.fr"
 GEC_MAIL_TO="gec@ac-reunion.fr"
 MAIL_USERNAME="gec@ac-reunion.fr"
 MAIL_USERPWD="aChangerDeSuite"
 
```

## **3 - Gestion des formulaires**
- - -

### 3.1 - Création d’un formulaire 
- - -

La création de formulaire peut se faire directement en accédant à l’espace de gestion via l’url **\[host\]/app/manage **sinon vous pouvez aussi écrire votre json dans un éditeur de texte externe. Puis coller le fichier dans le dossier **« app/forms »**

*   Choisir un identifiant pour le formulaire ( appeler aussi « slug »),
    *ex: CAND_2018TECHSMIB*

>    L’identifiant doit être en minuscule/majuscule, sans espace et sans caractère spéciaux. L’identifiant sert à la fois pour la configuration du formulaire et pour son appel pour l’affichage

   Reprendre le JSON du formulaire par défaut (app/forms/default.json) et amender selon son besoin (votre fichier deviens par exemple *app/forms/CAND\_2018TECHSMIB.json)*

```
{ // Début du JSON

"action" :"./process.php",

// Le fichier de traitement du formulaire.Ce fichier est repris
// dans l’attribut « action » de la balise html form
"date_begin" :"25-05-2017",
// Date de lancement du formulaire
"date_end" : "25-12-2017",
// Date de fin du formulaire
"mail_subject" : "AC REUNION - Candidature apprentissage",
// Sujet du mail transmis par le formulaire.
"mail_from_name" : "GEC Recrutement Apprenti 2017",
// Paramètre nom du mail
"mail_from" : "<gec_recrutement_apprentis@ac-reunion.fr>",
// Utilisateur du serveur SMTP
"mail_username" : "gec_recrutement_apprentis",
// Mot de passe de l’utilisateur du serveur SMTP
"mail_userpwd" : "xxxxxxxxxx",



//------------------------------
"form" : { "inputs": … ]},
//------------ détail en dessous 

"submit" : "Envoyer votre candidature"

}  // Fin du JSON


```

détails de « inputs »


```
"form" : { "inputs":
    [ {"id":"name", "label":"Nom :", "class":"none",
    "type":"input",
    "required":"false"},

    {"id":"firstname",
    "label":"Prénom(s) :",
    "class":"none",
    "type":"input",
    "required":"false"},

    {"id":"email",
    "label":"Adresse mèl :",
    "class":"none",
    "type":"input:email",
    "required":"false"},

    {"id":"phone-number",
    "label":"Numéro de téléphone :",
    "class":"none",
    "type":"input:number",
    "required":"false"},

    {"id":"postal-code",
    "label":"Code postal (lieu d'habitation) :",
    "class":"none",
    "type":"input:number",
    "required":"false"},

    {"id":"reference",
    "label":"Référence de l'offre :",
    "class":"none",
    "type":"select",
    "value" : {
        "1":"APPR2017-DSI-L3CSI",
        "2":"APPR2017-DSI-BTSSIO-SUPPORT",
        "3":"APPR2017-DSI-BTSSIO-INFRA",
        "4":"APPR2017-DSI-BTSAM",
        "5":"APPR2017-DPATE-BTSAM",
        "6":"APPR2017-BACPROGA",
        "7":"APPR2017-MCSEOP"},
        "required":"false"},

    {"id":"school-curriculum",
    "label":"Cursus scolaire (précisez les diplômes obtenus – 800 caractères maximum, 8 lignes max.)",
    "class":"none",
    "type":"textarea:800c:8l",
    "required":"false"},

    {"id":"motivation",
    "label":"Motivation (Décrivez votre motivation pour le poste - 800 car. max. 8 lignes max.)",
    "class":"none",
    "type":"textarea:800c:8l",
    "required":"false"},

    {"id":"professional-experience",
    "label":"Expériences professionnelles éventuelles (800 car. max. 8 lignes max.)", "class":"none",
    "type":"textarea:800c:8l",
    "required":"false"},

    {"id":"other",
    "label":"Autres éléments que vous voudriez porter à la connaissance de l'administration (800 car. max.)",
    "class":"none",
    "type":"textarea:800c:8l",
    "required":"false"}

]},
```



Les **options disponibles pour décrire un input** sont les suivantes :

```
"id":"name",
```
* Identifiant html du champ, certain id sont réservéS ex : « reference » qui contient obligatoirement les références des offres d’emploi*

```
"label":"Nom :"
```
* Label afficher en dessus du champ de saisie*

```
"class":"none",
```
* Classe CSS afin de personnaliser le champ*
 
```
"type":"input",
```
Pour le champ type, les valeurs disponibles sont :
```
 input, input:number, input:email,select, texterea:800C:8L
```
L’option *select* vient obligatoirement avec l’option *value*

```
"value" : {
    "1":"APPR2017-DSI-L3CSI",
    "2":"APPR2017-DSI-BTSSIO-SUPPORT",
    "3":"APPR2017-MCSEOP"
},
```
**800C** définit le nombre de caractère autoriser pour ce champ et 8L le nombre de ligne)

```
texterea:800C:8L
```


### 3.2 - Utilisation du formulaire
- - -

### Appel via l’identifiant du formulaire ( slug )

#### Le formulaire peuvent être appelé directement par son identifiant

    Ex : https://sve.ac-reunion.fr/app/public/index.php?[slug=default]

###  Appel via l’identifiant de l’offre ( ref )
Les formulaires peuvent être appelés directement par l’identifiant et avoir une référence passer en paramètre. Cette configuration peut-être utile dans le cas de formulaire de recrutement uniquement.

    Ex : https://sve.ac-reunion.fr/app/public/index.php?[slug=default&ref=APPR2017-DSI-L3CSI]
    
    
## **4 - Diagnostic des anomalies (Troubleshooting) **
- - -
    
Vérifiez :

    l'appartenance des fichiers à un utilisateur/groupe ayant des droits en écriture et lecture sur le dossier/fichiers "/applis/www-ct/julien/sveformmaker/app/forms"  (l'utilisateur est par défaut www-data:www-data ou apache:apache )
    les droits du dossier et des fichiers contenus dans "app/forms" doivent être 755 soit rwxr-xr-x


Ils nous restent à vérifier :

    la configuration php.ini
        Le parmètre  allow_url_fopen = On
    la configuration de SELinux
        httpd_can_network_connect et httpd_unified doit être en On.
        (la commande sudo sestatus -b | grep httpd_unified peut être utile pour cela.)


Dans le cas d'une configuration conforme, il faudra mettre dans la fonction getJSON($slug) dans le fichier app/vendor/AcRun/src/SVEForm.php
à la fin du bloc catch de la ligne 165, les lignes suivantes :

    print_r($errors);
    print_r(error_get_last());
    die(__FILE__ . __LINE__);
