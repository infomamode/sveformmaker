<?php 


require "../vendor/autoload.php";
use App\AcRun\SVEForm;
use App\AcRun\CustomPDF;

$dotenv = new Dotenv\Dotenv('../config');
$dotenv->load();


$trustedIp = ip2long($_ENV['SECUREIP']);
  if($trustedIp != $_SERVER['HTTP_CLIENT_IP']){
     echo "IP non reconnue";     die; } 

if (!file_exists('../app/manage/UNLOCK')) {     echo "Le fichier de sécurité n'existe pas";     die; }



if($_ENV['APP_MODE'] == 'dev'){
	error_reporting(E_ALL);
	ini_set('display_errors', -1);
}

date_default_timezone_set($_ENV['TIMEZONE']);

if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
           if (session_status() == PHP_SESSION_NONE) {
			   
									session_start();
			}
        } else {
           if(session_id() == '') {
								session_start();

						}
        }
    }

	unset($_SESSION['SVEForm']);

	$_SESSION['SVEForm'] = new SVEForm;
	$slug = isset($_GET['slug']) ? $_GET['slug'] : $_ENV['DEFAULT_FORM'];
	$data = $_SESSION['SVEForm']->getJSON($slug);

	unset($_SESSION['form_inputs']);
	unset($_SESSION['form_json']);

	$_SESSION['form_inputs'] = $data[0];
	$_SESSION['form_json'] = $data[1];
	$_SESSION['form_json']['slug'] = $slug;
	$now = date('d-m-Y');


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include 'themes/partials/head.php'; ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">



		
		<!-- sidebar --------------------------------------------- -->
		    <?php include 'themes/partials/sidebar.php'; ?>  
    
    <div id="content-wrapper" class="d-flex flex-column">
		<!-- content ---------------------------------------------- -->
        <?php include 'themes/partials/content.php'; ?>
    </div>




  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal---------------------------- -->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  
  
  <!-- Bootstrap core JavaScript-->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

  <!-- Core plugin JavaScript-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" integrity="sha256-H3cjtrm/ztDeuhCN9I4yh4iN2Ybx/y1RM7rMmAesA0k=" crossorigin="anonymous"></script>
  <!-- Page level plugins -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>

  
    <!-- Custom scripts for all pages-->
  <script src="../manage/themes/assets/js/custom.js"></script>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("json"), {
  mode: "application/json",
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true
});
  
  
  </script>


</body>

</html>
