

<!-- Main Content -->
      <div id="content">

        <!-- Topbar -->


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Tableau de bord</h1>
            <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Export </a>-->
          </div>

  


          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">



  <!-- Default view ----------------------------------------------------------------------------------------------------------------------------- -->
        <?php if(!isset($_GET['e']) || empty($_GET['e'])) :?>
          <div class="card shadow mb-4">
            
              
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Accueil gestion</h6>
            </div>
            
            <div class="card-body">

              <a href="./index.php?e=create" class="btn btn-secondary btn-icon-split" >
                    <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Créer un nouveau formulaire</span>
                  </a>
              <br/>
              
              <div class="table-responsive">
                
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  <thead>
                    <tr>
                      <th>N° </th>
                      <th>Identifiant (slug) </th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tfoot>
                    <tr>
                      <th>N° </th>
                      <th>Identifiant (slug) </th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  
                  <tbody>
                     <?php
                         $i = 1;
                         foreach (glob($_ENV['APP_PATH']."forms/*.json") as $filename) :
                           $name = explode('/',$filename);
                           $name = explode('.', $name[7]);
                         ?>



                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $name[0] ?></td>
                                <td>
                                  

                                <a href="./index.php?e=update&slug=<?php echo $name[0]?>" class="btn btn-primary btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                      <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Modifier</span>
                                  </a>


                                
                                <a href="../public/index.php?slug=<?php echo $name[0]?>" target="_blank" class="btn btn-primary btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                      <i class="fas fa-eye"></i>
                                    </span>
                                    <span class="text">Voir</span>
                                  </a>
                                
                                
                                </td>


                              </tr>
                        <?php $i++;	endforeach; ?>
                  </tbody>
                  
                </table>
                
              </div>
              
              
            </div><!-- /div.card-body -->
          </div>
	      <?php endif;?>
              
  <!-- Display create form ----------------------------------------------------------------------------------------------------------------------------- -->
              
	      <?php if(isset($_GET['e']) and $_GET['e'] == 'create') :?>
			
        <p class="mb-4"><a href="./index.php">Accueil gestion</a> >> Création de formulaire			</p>
              
          <br/>
          <form action="index.php?e=save" class="" method="post" enctype="multipart/form-data">
              <input id="e" name="e" type="hidden" value="save">
             
            <div class="form-group">  
              <label for="slug" style="font-size: 1.3em">Identifiant</label><br/>
              <input id="slug" name="slug"/>
            </div>
              
           
            <div class="form-group">  
               <label for="json" style="font-size: 1.3em">Structure JSON du formulaire</label><br/>
               <textarea id="json" name="json" rows="4" cols="50"></textarea>
            </div>
            
            <div class="form-group">
                 <br/><br/>
                 <button><span class="mif-checkmark"></span> Créer </button>
            </div>
          </form>

        <?php endif;?>
              
         <!--  Modify ------------------------------------------------------------------------------------------------------------------------------->
	
  <?php if(isset($_GET['e']) and $_GET['e'] == 'update') :?>
	<?php 
		$slug = isset($_GET['slug']) ? $_GET['slug'] : '';
		$file_path = $_ENV['APP_PATH']."forms/".$slug.'.json';
		$file = file_get_contents($file_path);	
		

	?>
	 <p class="mb-4">	<a href="./index.php">Accueil gestion</a> >> Modification d'un formulaire			</p>
      <br/><form action="index.php?e=save&slug=<?php echo $slug ?>" class="pure-form pure-form-stacked" method="post" enctype="multipart/form-data">
<input id="e" name="e" type="hidden" value="save">
			<label for="slug" style="font-size: 1.3em">Identifiant</label>
				<input id="slug" name="slug" value="<?php echo trim($slug) ?>" disabled/>
			<br/><br/>
			<label for="json" style="font-size: 1.3em">Structure JSON du formulaire</label>
				<textarea id="json" name="json" rows="30" cols="100"><?php echo trim($file) ?></textarea>

			 <div><br/><br/><button><span class="mif-checkmark"></span> Modifier </button></div>
			</form>
			
	<?php endif;?>
			
			
	<!-- Save data ------------------------------------------------------------------------- -->
<?php //var_dump($_GET);var_dump($_POST); ?>
	<?php if(isset($_POST['e']) and $_POST['e'] == 'save') :?>
			<?php
		$slug = isset($_GET['slug']) ? $_GET['slug'] : $_POST['slug'];
		$json = isset($_POST['json']) ? $_POST['json'] : '';
		$file_path = $_ENV['APP_PATH']."forms/".$slug.'.json';
	
			$hdl = fopen($file_path,"w+");
			fwrite($hdl, $json);
			fclose($hdl);
			?>

			<div> <p> Formulaire enregistrée</p>
        <br/><br/>
            <a href="<?php echo  $_SERVER["HTTP_REFERER"] ?>" class="btn btn-secondary btn-icon-split">
              <span class="icon text-white-50">
              <i class="fas fa-arrow-left" aria-hidden="true"></i>
              </span>
              <span class="text"> Continuer l'édition </span>
              </a>
            </div>
            
	<?php endif; ?>       
              
              
        
            </div>
          </div>
          
        </div> <!-- /.container-fluid -->

      </div><!-- End of Main Content -->
  
        <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>SVEFormMaker <?php echo "version" ?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
  
  
