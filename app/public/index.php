<?php 

require "../vendor/autoload.php";
use App\AcRun\SVEForm;
use App\AcRun\CustomPDF;

$dotenv = new Dotenv\Dotenv('../config');
$dotenv->load();

if($_ENV['APP_MODE'] == 'dev'){
	error_reporting(E_ALL);
	ini_set('display_errors', -1);
}

date_default_timezone_set($_ENV['TIMEZONE']);

if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
           if (session_status() == PHP_SESSION_NONE) {
			   
									session_start();
			}
        } else {
           if(session_id() == '') {
								session_start();

						}
        }
    }

	unset($_SESSION['SVEForm']);

	$_SESSION['SVEForm'] = new SVEForm;
	$slug = isset($_GET['slug']) ? $_GET['slug'] : $_ENV['DEFAULT_FORM'];
	$data = $_SESSION['SVEForm']->getJSON($slug);

	unset($_SESSION['form_inputs']);
	unset($_SESSION['form_json']);

	$_SESSION['form_inputs'] = $data[0];
	$_SESSION['form_json'] = $data[1];
	$_SESSION['form_json']['slug'] = $slug;
	$now = date('d-m-Y');


?>

<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php echo $_ENV['TITLE_VIEW_FORM']; ?>
	</title>
	<style type="text/css">	.form-error {color: rgb(185, 74, 72);}	</style>

</head>

<body style="background: #f9f9f9 url('../public/imgs/image-background-paper.png');">
	<div class="pure-g">
		<div class="pure-u-3-5" style="margin:0px auto;">


			<?php
			
			if($now > $_SESSION['form_json']['date_begin'] && $now <= $_SESSION['form_json']['date_end'] ){

			  echo '<h1>Ce formulaire n\'est plus disponible</h1><br/><span>Merci d\'avoir consult� cette annonce vous pouvez continuer votre visite sur notre site institutionnel</span>';die;
			}
			// Set default Captcha value to false
				$_SESSION['error-captcha'] = false;

			// --------- CSRF Token
			// Some basic guard against CSRF attack
				$_SESSION['SVEForm']->setCSRFToken();

			// -------- Error Captcha
				  if(isset($_SESSION['error-captcha']) && $_SESSION['error-captcha'] === true ) {
					   echo '<span style="color:#e73950"> Le r�sultat du captcha est incorrect. </span>'.PHP_EOL;
				  }

			// -------- Page Form 
				 $html = $_SESSION['SVEForm']->renderForm($_SESSION['form_inputs'], $_SESSION['form_json']);
				 echo $html;

			?>

		</div>
	</div>
</body>

<!-- Footer -->
<footer>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<script src="../public/js/form_rule_validation.js">
	</script>
</footer>

</html>