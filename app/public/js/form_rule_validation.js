
        // --- Standard rule
          $.validate({
            lang : 'fr',
            modules : 'date'
          });
          $('#school-curriculum').restrictLength( $('#max-length-school-curriculum') );
          $('#motivation').restrictLength( $('#max-length-motivation') );
          $('#professional-experience').restrictLength( $('#max-length-professional-experience') );
          $('#other').restrictLength( $('#max-length-other') );
  
  


        // --- Custom rule
        $('#reference').change(function(){  
            if($(".refInfos").length > 0){  $(".refInfos").remove();  }                                                                                                                              
            console.log('Change select reference');                            
                var ref = $(this).val(); 
                var slug = 'apprentissage';
                   $.ajax({                                                                                                                                                                   
                          type: 'GET',                                                                                                                                                        
                          url: '../storage/data/forms/'+slug+'/'+ref+'.txt', // This is the url that will be requested                                                                                            
                          cache: false,
                          dataType: 'html',
                          success: function(html){                                                                                                           

                                $("#reference").after('<div class="refInfos"> <strong>Rappel des critéres de sélection : </strong>'+html+'</div>');                                                                            
                                                                                                                                                                                              
                           },                                                                                                                                                                 
                                                                                                                                                                            
                        });                                                                                                                                                                   
                                                                                                                                                                                              
        });    