<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Transmission de votre candidature</title>
</head>
<body style="background: #f9f9f9 url('../public/imgs/image-background-paper.png');">


<?php

	
require "../vendor/autoload.php";

use App\AcRun\SVEForm;
use App\AcRun\CustomPDF;
use App\AcRun\Db;
use \PDO;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$dotenv = new Dotenv\Dotenv('../config');
$dotenv->load();
	
	if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
           if (session_status() == PHP_SESSION_NONE) {	session_start(); }
        } else {
           if(session_id() == '') { session_start(); }
        }
    }

	
// -------- Check CSRF Token
	$chkCSRF = $_SESSION['SVEForm']->checkCSRFToken($_SESSION['token']);
// -------- Check Captcha
	$_SESSION['SVEForm']->CheckCaptcha($_POST['checking'], $_POST['nsum']);
// -------- Check date of birth
	$chkDateBirth = $_SESSION['SVEForm']->CheckDateBirth(25);
	$ref = $_SESSION['SVEForm']->ref;
	$chkUpload = $_SESSION['SVEForm']->uploadsAnnexes($_FILES);
//  -------- Prepare inputs from post request
	$data = $_SESSION['SVEForm']->arrangePostData($_POST, $_SESSION['form_inputs']);

	
	$pdfData = $data[0];
	$elm = array('ref' => array('label' =>'Réf :', 'value' => $ref));
	$pdfData = array_merge($elm, $pdfData);
	//var_dump($pdfData);die;
	
	$traceData = $data[1];

// Manage PDF
//-----------------------------------------------------------------------
	$pdf = new CustomPDF('P', 'mm', 'A4');
	//var_dump($pdf);die;
	$res = $_SESSION['SVEForm']->renderPDF($pdf, $pdfData, $_SESSION['form_json']['slug']);

// Manage mail
//-----------------------------------------------------------------------
    $mail = new PHPMailer;
		$check = $_SESSION['SVEForm']->sendToGEC($mail, $res['filename']);
		//$check = true;
		if(!$check) 
		{
		  echo "Une erreur dans l'envoi du mail : " . $mail->ErrorInfo ;
			echo "<br/>Veulliez contacteR le support technique via : webmaster@ac-reunion.fr";
		} 
		else 
		{
		    echo "<div style='text-align:center;font-family:Arial'><h2>Votre candidature est bien transmise pour traitement</h2>";
		    echo '<br/> Ci-après le récapitulatif au format PDF : <a href="../tmp/'.$res['file'].'">Télécharger</a></div>';
		}
	
	if($_ENV['SEND_AR'] == true){
		$AR = new PHPMailer;
		$_SESSION['SVEForm']->sendAR($AR, $res['filename'], $traceData['email'] );
	}


		
	
	
	// Finally, destroy the session.
	session_unset();
	session_destroy();
	session_write_close();
?>

</body>
<footer>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
</footer>
</html>				
