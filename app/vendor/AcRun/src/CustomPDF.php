<?php
namespace App\AcRun;

class CustomPDF extends \FPDF
	{
	
		protected $outlines = array();
		protected $outlineRoot;
	
	
	// Page header
	function Header()
		{

	if ($this->page == 1) {
		    // Logo
		    $this->Image($_ENV['URL_LOGO_ACADEMIE'],10,6,30);
		    // Arial bold 15
		    $this->SetFont('Arial','B',15);
		    // Move to the right
		    $this->Cell(80);
		    // Title
		    $this->Cell(80,10,$_ENV['TITLE_PDF'],1,0,'C');
		    // Line break
		    $this->Ln(20);
			}
		}

	// Page footer
	function Footer()
		{
		    // Position at 1.5 cm from bottom
		    $this->SetY(-15);
		    // Arial italic 8
		    $this->SetFont('Arial','I',8);
		    // Page number
		    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	
	
	
	
	


	function Bookmark($txt, $isUTF8=false, $level=0, $y=0)
	{
		 // if(!$isUTF8)
			 //   $txt = utf8_encode($txt);
			if($y==-1)
					$y = $this->GetY();
			$this->outlines[] = array('t'=>$txt, 'l'=>$level, 'y'=>($this->h-$y)*$this->k, 'p'=>$this->PageNo());
	}

	function _putbookmarks()
	{
			$nb = count($this->outlines);
			if($nb==0)
					return;
			$lru = array();
			$level = 0;
			foreach($this->outlines as $i=>$o)
			{
					if($o['l']>0)
					{
							$parent = $lru[$o['l']-1];
							// Set parent and last pointers
							$this->outlines[$i]['parent'] = $parent;
							$this->outlines[$parent]['last'] = $i;
							if($o['l']>$level)
							{
									// Level increasing: set first pointer
									$this->outlines[$parent]['first'] = $i;
							}
					}
					else
							$this->outlines[$i]['parent'] = $nb;
					if($o['l']<=$level && $i>0)
					{
							// Set prev and next pointers
							$prev = $lru[$o['l']];
							$this->outlines[$prev]['next'] = $i;
							$this->outlines[$i]['prev'] = $prev;
					}
					$lru[$o['l']] = $i;
					$level = $o['l'];
			}
			// Outline items
			$n = $this->n+1;
			foreach($this->outlines as $i=>$o)
			{
					$this->_newobj();
					$this->_put('<</Title '.$this->_textstring($o['t']));
					$this->_put('/Parent '.($n+$o['parent']).' 0 R');
					if(isset($o['prev']))
							$this->_put('/Prev '.($n+$o['prev']).' 0 R');
					if(isset($o['next']))
							$this->_put('/Next '.($n+$o['next']).' 0 R');
					if(isset($o['first']))
							$this->_put('/First '.($n+$o['first']).' 0 R');
					if(isset($o['last']))
							$this->_put('/Last '.($n+$o['last']).' 0 R');
					$this->_put(sprintf('/Dest [%d 0 R /XYZ 0 %.2F null]',$this->PageInfo[$o['p']]['n'],$o['y']));
					$this->_put('/Count 0>>');
					$this->_put('endobj');
			}
			// Outline root
			$this->_newobj();
			$this->outlineRoot = $this->n;
			$this->_put('<</Type /Outlines /First '.$n.' 0 R');
			$this->_put('/Last '.($n+$lru[0]).' 0 R>>');
			$this->_put('endobj');
	}

	function _putresources()
	{
			parent::_putresources();
			$this->_putbookmarks();
	}

	function _putcatalog()
	{
			parent::_putcatalog();
			if(count($this->outlines)>0)
			{
					$this->_put('/Outlines '.$this->outlineRoot.' 0 R');
					$this->_put('/PageMode /UseOutlines');
			}
	}

	
	
	
	
	
	
	
	
	
	}
	
?>