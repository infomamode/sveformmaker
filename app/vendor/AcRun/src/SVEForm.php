<?php 

namespace App\AcRun;



class SVEForm {
	
	
	public $ref;
	public $version;
	
	function __construct(){
	
		$this->ref = $this->getToken(25);
		$this->version = 'beta-1.4';

	}

	 /**
     * Set CSRF Token 
     * @returm true or $errors array
     */
	
	public function setCSRFToken(){
		
      if (empty($_SESSION['token'])) {
          if (function_exists('mcrypt_create_iv')) {
              // Php 7
              $_SESSION['token'] = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
						
						return true;
          } else {
              // Php 5
              $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32));
						
						return true;
          }
		  
      } else {
		
		  	return $errors[] = "Token CSRF error : session token is empty";
	  	}
		
	}
	
	

	
	 /**
     * Check CSRF token value
     * @param $token string value
     */
	
	public function checkCSRFToken($token){
		
		if (!empty($token)) {
			
				if(!function_exists('hash_equals')) {
					$checkCSRF = $this->hash_equals($_SESSION['token'], $token);
				} else{
					$checkCSRF = hash_equals($_SESSION['token'], $token);
				}
				
				if ($checkCSRF) {
					$validToken = true; 
					return true;
				} else {
					$validToken = false;
					exit("Token CSRF error : token invalid");
					// TODO : Log this as a warning and keep an eye on
				}
			
		} else {
		
		  	return $errors[] = "Token CSRF error : session token is empty";
	 	 }
		
	}

	 /**
     * Check Captcha
     * @param $userVal integer value
     * @param $nsum integer value
     */
	
	public function CheckCaptcha($userVal, $nsum) {
		
		if($userVal !== $nsum ){
				$_SESSION['error-captcha'] = true;
				header('Location:  ./index.php');
				exit();
			}

	}

	/**
     * Check date of birth
     * @param $ageLimit integer value
     */
	
	public function CheckDateBirth($ageLimit) {
		
		if(isset($_POST['dateofbirth'])){

				$dateofbirth = explode('/', $_POST['dateofbirth']);
				if($dateofbirth[2] < (date("Y") - $ageLimit)){

					echo "<div style='text-align:center;font-family:Arial'><h2>Votre candidature ne peut être prise en compte.</h2>";
					echo '<br/>Vous ne remplissez pas les conditions de limite d\'âge '.$ageLimit.' ans</div>';
					exit();
				} else {
					
					return TRUE;
					
				}
			
			
			} else {
			
			return $errors[] = "Date of birth not set";
		}

	}

	/**
   * Get JSON data from slug filename 
   * @param $slug string value
   */
	
	public function getJSON($slug) {
		
		$errors = array();
		$form_path = $_ENV['APP_PATH'] . "forms/$slug.json";
		
		try {
			$str = file_get_contents($form_path);

		} catch (Exception $e) {
			$errors[] = 'Error from file_get_contents : '.$e->getMessage();
		}
		
		if($str){
			if(!function_exists('json_decode')){
				$json = json_decode($str, true);
				$inputs = $json['form']['inputs'];
			} else {
				$errors[] = 'Json decode return errors ';
			}
			
		} else{
			$errors[] = 'Error from file_get_contents result $str ';
		}

		$res = count($errors)>0 ? false : array($inputs, $json) ;
		return $res ;
		
	}

	 /**
     * Generate form HTML
     *
     * @param $inputs array
	 * @param $json array
     */
	
	public function renderForm($inputs, $json){
	  if(!isset($inputs) && empty($inputs)){
	  	$html = "Erreur : le formulaire est vide, veuillez le signaler à l'adresse webmaster@ac-reunion.fr";
		return $html;
		die;
	  }
	  $html = 	
	  $html = '<h1>'. $_ENV['TITLE_VIEW_FORM'] . '</h1>';
	  $jsonAction = isset($json['action']) ? $json['action'] : '' ;
      $html .= '<br/><form action="' . $jsonAction . '" class="pure-form pure-form-stacked" method="post" enctype="multipart/form-data">';
	
      foreach($inputs as $i)
      {

            $id = $i['id'];
            $label = $i['label'];
            $type = $i['type'];
			$type = explode(':', $type);
            $class = $i['class'];
            $value = isset($i['value']) ? $i['value'] : '' ;
            $required = $i['required'];
            
            $html.= '<div class="pure-control-group pure-u-5-5">';
            $html.= '<label for="'.$id.'" style="font-size: 1.3em">' . $label . '</label>';

            if ($type[0] == 'input') {
                  $typeVal = isset($type[1]) ? $type[1] : '';

                  $html.= '<input id="' . $id . '" name="'.$id.'" type="text" data-validation="'.$typeVal.'" placeholder="" class="pure-input-1">'.PHP_EOL;
                  

                    
              } elseif ($type[0] == 'textarea') {

                    $html.= ' <span style="color:#c59f9f"><span id="max-length-' . $id . '" style="">800</span> caractères restants</span></br>';
                    $html.= ' <textarea id="' . $id . '" name="'.$id.'" rows="8" placeholder="" class="pure-input-1"  
							data-validation="length" data-validation-length="max800"></textarea>'.PHP_EOL;
                    
              } elseif ($type[0] == 'select') {

                    $html.= '<select id="' . $id . '" name="'.$id.'" class="pure-input-1">'.PHP_EOL;

                    foreach($value as $v)
                    {
                          $html.= '<option value="'.$v.'">' . $v . '</option>'.PHP_EOL;
													 if(isset($_GET['ref_offre'])){
															if($v == $_GET['ref_offre'] )
																					$html.= '<option value="'.$v.'" selected>' . $v . '</option>'.PHP_EOL;
													 }
                         
                    }

                    $html.= '</select>'.PHP_EOL;
										
					// Custom 
                    if($id == 'reference'){
                      
                      $html.= '<label for="name" style="font-size: 1.3em"> Date de naissance : </label>';
                      $html.= '<input id="dateofbirth" name="dateofbirth" type="text" data-validation="birthdate" 
											data-validation-format="dd/mm/yyyy" data-validation-help="La date doit être au format dd/mm/yyyy - 
											le candidat doit avoir moins de 26 ans"placeholder="" class="pure-input-1">'.PHP_EOL;

                    }
							
              } elseif ($type[0] == 'radio') {


                    foreach($value as $v)
                    {
                          $html.= '<option value="'.$v.'">' . $v . '</option>'.PHP_EOL;

                         
                    }

                    $html.= '</select>'.PHP_EOL;
							
							} elseif ($type[0] == 'checkbox') {
							
								    $html.= '<select id="' . $id . '" name="'.$id.'" class="pure-input-1">'.PHP_EOL;

                    foreach($value as $v)
                    {
                          $html.= '<option value="'.$v.'">' . $v . '</option>'.PHP_EOL;
													 if(isset($_GET['ref_offre'])){
															if($v == $_GET['ref_offre'] )
																					$html.= '<option value="'.$v.'" selected>' . $v . '</option>'.PHP_EOL;
													 }
                         
                    }

                    $html.= '</select>'.PHP_EOL;
							
							 }
				

				
            $html.= '</div>'.PHP_EOL;
      }

	//------------ Set attachments inputs
				
				    $html.= ' <br/><div class="col-md-8">';
								$html.= ' <label for="" style="font-size: 1.3em">Joindre vos annexes (5 max. au format JPEG/GIF/PNG de -2Mo)</label>';
								for($i=1; $i <= $_ENV['NBR_ATTACHMENTS']; $i++){

										$html.= '<div class="mb10">';
										$html.= '  <input id="input-upload-img'.$i.'" name="input-upload-img[]" type="file" class="file" data-preview-file-type="text">';
										$html.= '</div>';
								}
		
            $html.= ' </div><br/>';
		
	// -------- set Captcha
      $n1 = rand(1,10); $n2 = rand(1,10); $nsum = $n1 +$n2;

      $html.= '<hr><label for="checking" class="">Captcha : saisissez dans ce champ le resultat de ' .$n1.'+' .$n2.' : </label>';
      $html.= '<input type="text" name="checking" id="checking" class="" value="" />';
      $html.= '<br/><input type="hidden" id="nsum" name="nsum" value="'.$nsum.'"/><div class="pure-control-group pure-u-1-5" style="">
	  			<button type="submit" class="pure-button pure-button-primary button-success">';
				
	  $jsonSubmit = isset($json['action']) ? $json['action'] : 'Envoyer' ;
	  $html.= '<span class="mif-checkmark"></span>' . $jsonSubmit . '</button></div></form>'.PHP_EOL;
				

		return $html;
	}

	
	
	
	
	
	
	
	 /**
     * Upload files from POST request
     *
     * @param $files array $_FILES
	   *  
     */
	
	public function uploadsAnnexes($files){


		$date = date('Y-m-d');
		$file_path =  $target_dir = $_ENV['APP_PATH'] . '/storage/data/files/'.$this->ref.'/';
		

		$statusUpload = array();
		
		// Images
		//------------------------------------------------------------------------------
		
		if (!file_exists($file_path)) {
			mkdir($file_path, 0755, true);
		}
		
		
		$countFile= count($_FILES["input-upload-img"]["name"]);

			for($i=0; $i<$countFile; $i++){
				
				if(empty($_FILES["input-upload-img"]["tmp_name"][$i])){
					break;	
				};
				$check = getimagesize($_FILES["input-upload-img"]["tmp_name"][$i]);
				
				$name = preg_replace("/[^A-Z0-9._-]/i", "_", $_FILES["input-upload-img"]["name"][$i]);
				$name = $date.'_'.$name;
				$target_file = $target_dir .'/'. $name;


				//---  Check if image file is a actual image or fake image
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					
					if(!empty($_FILES["input-upload-img"]["tmp_name"][$i]) ){
					
						$imageFileType = $check["mime"];
						$statusUpload['success']['img'] = true;

					} else {

						$statusUpload['errors']['img'][] = "File is empty.";
					};

				}

			
				//---  Check file size 2Mb
				if( $_FILES["input-upload-img"]["size"][$i] > $_ENV['MAX_SIZE_FILES'] ) {
					$statusUpload['errors']['img'][] = "Sorry, your file is too large.";
				}
				
				//---  Allow certain file formats
				if(isset($imageFileType) && $imageFileType != "image/jpg" && $imageFileType != "image/png" 
				   && $imageFileType != "image/jpeg" && $imageFileType != "image/gif" ) {
					$statusUpload['errors']['img'][] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				}
				
				//---  Check if $uploadOk is set to 0 by an error
				if (isset($statusUpload['errors']) && count($statusUpload['errors']['img']) > 0) {
					$statusUpload['errors']['img'][] = "Final img errors";break;
					
				//---  if everything is ok, try to upload fil
				} else {
					
					if (move_uploaded_file($_FILES["input-upload-img"]["tmp_name"][$i], $target_file)) {
						//Nothing
					} else {

						$statusUpload['errors']['img'][] = "Sorry, there was an error moving your file on file system.";
					}
					
					$statusUpload['success']['img'] = true;
				}
				
				
		
			}

		
		
				return $statusUpload;
		
	}
	
	
	
	 /**
     * Render pdf file
     *
     * @param $pdf FPDF object
	 * @param $data array
	 * @param $slug string
     */

	public function renderPDF($pdf, $data, $slug){
		
		ob_end_clean();
		ob_start();

	
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetFont('Arial','',12);

		
		foreach ($data as $d) {
			$label = iconv('UTF-8', 'windows-1252', $d['label']);
			$pdf->MultiCell(0,5,$label,'B');

			$pdf->Ln();
			$value = iconv('UTF-8', 'windows-1252', $d['value']);
			$pdf->MultiCell(0,5,$value);
			$pdf->Ln();
		}

		$date = date('Y-m-d_H-i-s');
		$file = $date.' '.$_ENV['ID_ACADEMIE'].'_'.$slug.'.pdf';
		$filename = $_ENV['APP_PATH'].'tmp/'.$file;

		
		
		$file_path = $_ENV['APP_PATH'] . 'storage/data/files/'.$this->ref.'/';
		$i=0;
		
		foreach (array_filter(glob($file_path.'*'), 'is_file') as $file)
		{
			
			list($width, $height) = getimagesize($file);

			if ($width > $height) {
					$orientation = "L"; 
					$w = 290 ;
					$h = 205;
			}
			else {
					$orientation = "P"; 
					$w = 205 ;
					$h = 290;
			}
			
				$pdf->AddPage($orientation);
				$pdf->Bookmark('Annexe '.$i, false);
			
			$ext = pathinfo($file, PATHINFO_EXTENSION);

			
				$pdf->Image($file,3,3,$w,$h);
				$i++;
		}
		
		$file = end(explode('/',$filename));

		$pdf->Output($filename, 'F');
		ob_end_flush();

		return array('file' => $file, 'filename' => $filename);
	}

	 /**
     * Arrange POST request data correctly for PDF rendering
     *
     * @param data array
	 * @param inputs array
     */
	
	public function arrangePostData($data, $inputs){
		// Unset trivial input
			unset($data['checking']);
			unset($data['nsum']);

			$all = array();

		// Get french label for input 
			foreach ($inputs as $k)
			{
				$all[$k['id']]['label'] = $k['label'];
			}

			$all['dateofbirth']['label'] = 'Date de naissance';

		// Value for input
			foreach($data as $k => $v)
			{
					$all[$k]['value'] = isset($data[$k])  ? $v : '';               
			}
			
			// TODO : Est-ce utile ????
			foreach($data as $k => $v)
			{
					$d[$k] = isset($data[$k])  ?  $v : '';               
			}
			

		return array($all, $d);
	}


	
	 /**
     * Lorem ipsum dolor sit amet
     *
     * @var string
     */
	
	public function sendToGEC($mail, $filename)	{
    
    // ---------- SMTP config --------------
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 4; // enables SMTP debug information (for testing) 1 = errors and messages
	  $mail->SMTPAuth   = $_ENV['MAIL_SMTP_AUTH']; // enable SMTP authentication
		$mail->Host       = $_ENV['MAIL_SMTP_HOST']; // sets the SMTP server
		$mail->Port       = $_ENV['MAIL_SMTP_PORT']; // set the SMTP port for the GMAIL server
		$mail->SMTPSecure = 'ssl';
		$mail->Username   = $_ENV['MAIL_USERNAME']; // SMTP account username 
		$mail->Password   = $_ENV['GEC_MAIL_USERPWD']; // SMTP account password
		
    // ---------- Sender config --------------
    $mail->From = $_ENV['GEC_MAIL_FROM'];
		$mail->FromName = $_SESSION['form_json']['mail_from_name'];
		$mail->addAddress($_ENV['GEC_MAIL_TO'], $_SESSION['form_json']['mail_from_name']);
		//Provide file path and name of the attachments
		$mail->addAttachment($filename);
		//Support Bcc 
		$mail->addBCC($_ENV['SUPPORT_MAIL']);      
		$mail->isHTML(true);

    // ---------- Mail content config --------------
		$mail->Subject = $_SESSION['form_json']['mail_subject'];
		$mail->Body = "<i>Un nouvel envoi via le formulaire de saisine";
		$mail->AltBody = "Un nouvel envoi via le formulaire";

		//var_dump($mail);
		$check = $mail->send(); 
		
		return $check;
	}
	
	
		
	
	 /**
     * Lorem ipsum dolor sit amet
     *
     * @var string
     */
	
	public function sendAR($mail, $filename, $to) {
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->SMTPDebug  = 4; // enables SMTP debug information (for testing) 1 = errors and messages
		$mail->SMTPAuth   = $_ENV['MAIL_SMTP_AUTH']; // enable SMTP authentication
		$mail->Host       = $_ENV['MAIL_SMTP_HOST']; // sets the SMTP server
		$mail->Port       = $_ENV['MAIL_SMTP_PORT']; // set the SMTP port for the GMAIL server
		$mail->SMTPSecure = 'ssl';
		$mail->Username   = $_ENV['MAIL_USERNAME']; // SMTP account username
		$mail->Password   = $_ENV['MAIL_USERPWD']; // SMTP account password
    
		$mail->From = 'nepasrepondre@ac-reunion.fr';
		$mail->FromName = 'Rectorat de la Reunion';
		$mail->addAddress($to, 'Rectorat de la Reunion');
		//Provide file path and name of the attachments
		$mail->addAttachment($filename);
		$mail->isHTML(true);

		$mail->Subject = 'Avis d\'enregistrement de votre saisine';
		$mail->Body = "Bonjour ".$_POST['name']." ". $_POST['firstname'].", <br/>Votre message a bien été enregistré le " . date("d/m/Y") . " à " . date("H:i") . " (heure de la Réunion). <br/>Cordialement,<br/>Rectorat de la Reunion ";
		$mail->AltBody = "Bonjour, votre message a bien été enregistré.Votre message a bien été enregistré le " . date("d/m/Y") . " à " . date("H:i") . " (heure de la Réunion). ";

		//var_dump($mail);
		$check = $mail->send(); 
		
		return $check;
		
	}
	
	
	 /**
     * Insert data in sql table 'trace' to keep SVE Dashboard uptodate
     *
     * @param $db oject 
	   * @param $traceData array 
     * @param $theme integer  
     * @param $stheme interger 
     */
	
	public function insertTrace($db, $traceData, $theme=14, $stheme=30){
	
		 $trace = array(
				// 14 : Concours, emploi, carriére, 30 : Recrutement
						'theme' => $theme,	 
						'sousTheme' => $stheme, 
				// 2 : Mr , 1 : Mme
						'civilite' => 0,	 
						'prenom' => $traceData["name"], 
						'nom' => $traceData["firstname"], 
						'adrmel' => $traceData["email"],
						'message' => $traceData["school-curriculum"].' '.$traceData["motivation"].' '.$traceData["professional-experience"],
						'datemel' => date('Y-m-d H:i'),
						'mailDst' => $_ENV['MAIL_FROM'],
						'mailLog' => 0, 
						);
		
		$insertId = $db->create( 'trace', $trace ); 
		
		return $insertId;
	}

	 /**
     * Generate a more secure rand value
     *
	   * @param $min integer 
	   * @param $max integer 
     */
  
  	public function randSecure($min=10, $max=46)
	{
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd > $range);
		
		return $min + $rnd;
	}
  
	
	 /**
     * Generate an alphanumeric token with $lengh character
     *
     * @param $lengh integer
     */
	
	public function getToken($length)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHJKLMPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghjkmnpqrstuvwxyz";
		$codeAlphabet.= "23456789";
		$max = strlen($codeAlphabet); // edited

		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->randSecure(0, $max-1)];
		}

		return $token;
	}
	
	
} // end class


?>
